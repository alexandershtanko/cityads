package alexandershtanko.com.cityads.exception;

/**
 * Created by aleksandr on 06.10.15.
 */
public class ConnectionException extends Exception {
    public ConnectionException(Exception e) {
        super(e);
    }

    public ConnectionException(String message) {
        super(message);
    }
}
