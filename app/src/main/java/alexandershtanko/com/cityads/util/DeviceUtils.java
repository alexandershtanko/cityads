package alexandershtanko.com.cityads.util;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by aleksandr on 05.10.15.
 */
public class DeviceUtils {
    public static String getIMSI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getSubscriberId();
    }
}
