package alexandershtanko.com.cityads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import alexandershtanko.com.cityads.model.AdsResponse;
import alexandershtanko.com.cityads.net.ConnectionHelper;
import alexandershtanko.com.cityads.util.DeviceUtils;

/**
 * Created by aleksandr on 05.10.15.
 */
public class AdsActivity extends Activity {

    private static final String TAG = "AdsActivity";

    private WebView webView;
    private ProgressBar progressBar;
    private ConnectionHelper connectionHelper = new ConnectionHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ads_activity_layout);
        initView();
        makeRequest();

    }

    private void initView() {
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        WebViewClient webViewClient = new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null) {
                    openInBrowser(url);
                    return true;
                } else {
                    return false;
                }
            }
        };

        webView.setWebViewClient(webViewClient);
        webView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

    }


    private void makeRequest() {
        (new AsyncRequest()).execute();
    }

    private void showMessage(String message) {
        new AlertDialog.Builder(AdsActivity.this)
                .setTitle(R.string.dialog_title)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                })
                .show();
    }

    private void openUrl(String url) {
        progressBar.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
        webView.loadUrl(url);
    }

    private void openInBrowser(String url) {
        if (url != null)
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        finish();
    }





    private class AsyncRequest extends AsyncTask<Void, Void, AdsResponse> {
        @Override
        protected AdsResponse doInBackground(Void... voids) {
            try {

                String id = DeviceUtils.getIMSI(getApplicationContext());
                return connectionHelper.makeAdsRequest(id);

            } catch (Exception e) {
                Log.e(TAG, "", e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(AdsResponse result) {
            if (result != null) {
                if (result.isOk()) {
                    openUrl(result.url);
                } else {
                    showMessage(result.message);
                }
            } else {
                finish();
            }

            super.onPostExecute(result);
        }
    }

}
