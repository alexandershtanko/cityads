package alexandershtanko.com.cityads.model;

/**
 * Created by aleksandr on 05.10.15.
 */
public class AdsResponse {
    public String status;
    public String message;
    public String url;

    public boolean isOk() {
        return (status != null && status.equals("OK"));
    }
}
