package alexandershtanko.com.cityads.net;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import alexandershtanko.com.cityads.exception.ConnectionException;
import alexandershtanko.com.cityads.model.AdsResponse;

/**
 * Created by aleksandr on 05.10.15.
 */
public class ConnectionHelper {

    private static final String API_URL="http://www.505.rs";
    private static final String API_REQUEST="/adviator/index.php";
    private static final String TOKEN="abd93bb8-2857-2fd0-7679-0b25087e1d35";

    private static final String REQUEST_PARAM_ID = "id";

    private final OkHttpClient client;


    public ConnectionHelper()
    {
        client = new OkHttpClient();

    }

    public AdsResponse makeAdsRequest(String id) throws ConnectionException {
        try {
            RequestBody formBody = new FormEncodingBuilder()
                    .add(REQUEST_PARAM_ID, id)
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Postman-Token", TOKEN)
                    .url(getUrl(API_REQUEST))
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) throw new ConnectionException("Invalid response:"+response.code());

            String responseJson = response.body().string();

            ObjectMapper objectMapper = new ObjectMapper();

            return objectMapper.readValue(responseJson, AdsResponse.class);
        }
        catch (Exception e)
        {
            throw new ConnectionException(e);
        }

    }

    private String getUrl(String path) {
        return API_URL+path;
    }

}
